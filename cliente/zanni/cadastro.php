<?php
include("conexao.php");
session_start();
$enviar = null;
$confirmar = null;
?>

<?php
	if($_SERVER['REQUEST_METHOD'] == "POST"){
		
		$sql_code = "SELECT * FROM cliente WHERE (user1 = '$_POST[user]' AND password1 = '$_POST[password]')";

		$sql_query = $mysqli->query($sql_code) or die ($mysqli->erro);
	  
		$linha = $sql_query->fetch_assoc();
		$page = $linha['pageName'];

		if($linha['acessStatus'] == "ok"){
			echo "<script> alert('Welcome.'); location.href='/cliente/$page'; </script>";
		} 
		else{
			echo '<script>alert("Not able to login. User not found or password incorrect.")</script>';
		}
	}
?>

<!DOCTYPE HTML>
<!--
	Strongly Typed by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>SOVA IT Co.</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" >
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="no-sidebar is-preload">
		<div id="page-wrapper">

			<!-- Header -->
				<section id="header">
					<div class="container">

						<!-- Logo -->
							<h1 id="logo"><a href="index.html">SOVA IT Co.</a></h1>
							<p>Empresa de T.I. focada em desenvolver as melhores soluções de tecnologia para empresas.</p>


					</div>
				</section>

			<!-- Main -->
				<section id="main">
					<div class="container">
						<div id="content">

							<!-- Post -->
								<article class="box">
								<div class="row">
							<div class="col-6 col-12-medium">
								<section>
									<form role="form" method="post">
										<div class="row gtr-50">
										<p>Acesso ao seu site com usuário e senha:</p>
											<div class="col-6 col-12-small">
												<input name="user" placeholder="User" type="text" required>
											</div>
											<div class="col-6 col-12-small">
												<input name="password" placeholder="Password" type="text" required>
											</div>
											<div class="col-12">
												<input type="submit" class="form-button button icon" name="login" id="login" value="Login">
											</div>
										</div>
									</form>
								</section>
							</div>
								</article>

						</div>
					</div>
				</section>

			<!-- Footer -->
				<section id="footer">
					
					<div id="copyright" class="container">
						<ul class="links">
							<li>&copy; SOVA IT Co. All rights reserved.</li><li>Design: <a href="http://html5up.net">SOVA IT Co.</a></li>
						</ul>
					</div>
				</section>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>